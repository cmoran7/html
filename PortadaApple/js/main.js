/* Seleccion por ID el contenedor de todo el menu a stikear */
var menu = document.querySelector('#main-header');
//Seleccionamos la altura actual del menu
var altura = menu.offsetTop;
function stikearMenu() {
	if (window.pageYOffset > altura) {
		menu.classList.add('fixed-menu');
	} else {
		menu.classList.remove('fixed-menu');
	}
}
window.addEventListener('scroll', stikearMenu);